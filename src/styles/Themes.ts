export interface Theme {
    BACKGROUND: string,
    TEXT: string,
    ACCENT: string,
    DIVIDER: string
}

enum PrimaryTheme {
    BACKGROUND = '#05dfd7',
    TEXT = '#ec7373',
    ACCENT = '#d8b5b5',
    DIVIDER = '#ffe196'
}

enum SecondaryTheme {
    BACKGROUND = '#f6e5f5',
    TEXT = '#fbf4f9',
    ACCENT = '#f6e7e6',
    DIVIDER = '#b9cced'
}

export class Themes {
    static readonly primaryTheme: Theme = PrimaryTheme;
    static readonly secondaryTheme : Theme = SecondaryTheme;
}
