import * as React from 'react';
import {Button, SafeAreaView, Text, View} from 'react-native';
import {useTheme} from "../context/ThemeProviderContext";
import {Theme, Themes} from "../styles/Themes";


interface Props {
    theme: Theme;
    setTheme: any;
}

class Home extends React.Component<Props, any> {
    constructor(props) {
        super(props);
        console.log(props.theme);
    }

    render() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    alignItems: 'stretch',
                    justifyContent: 'flex-start',
                }}>
                <View style={{width: '100%', height: '15%', backgroundColor: this.props.theme.ACCENT}}/>
                <View style={{width: '100%', height: '15%', backgroundColor: this.props.theme.BACKGROUND}}/>
                <View style={{width: '100%', height: '15%', backgroundColor: this.props.theme.DIVIDER}}/>
                <View style={{width: '100%', height: '15%', backgroundColor: this.props.theme.TEXT}}/>
                <Button title={'Set Secondary Theme'} onPress={() => {
                    this.props.setTheme(Themes.secondaryTheme);
                }}/>
                <Button title={'Set Primary Theme'} onPress={() => {
                    this.props.setTheme(Themes.primaryTheme);
                }}/>
            </SafeAreaView>
        );
    }
}

export default useTheme(Home);
