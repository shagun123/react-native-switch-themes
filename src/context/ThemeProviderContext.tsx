import * as React from "react";
import {Themes} from "../styles/Themes";

const ThemeContext = React.createContext<any>({});

export const ThemeContextProvider = (props) => {
    const [selectedTheme, setTheme] = React.useState(Themes.primaryTheme);
    return (
        <ThemeContext.Provider value={{theme: selectedTheme, setTheme: setTheme}}>
            {props.children}
        </ThemeContext.Provider>
    )
};

export function useTheme(Component) {
    return props => {
        const {theme, setTheme} = React.useContext(ThemeContext);

        const updateTheme = theme => {
            setTheme(theme);
        };
        return <Component {...props} theme={theme} setTheme={updateTheme}/>
    };
}
