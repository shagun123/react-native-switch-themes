import * as React from 'react';
import Home from './src/screens/Home';
import {ThemeContextProvider} from "./src/context/ThemeProviderContext";

class App extends React.Component {
  render() {
    return <ThemeContextProvider>
      <Home />
    </ThemeContextProvider>;
  }
}

export default App;
